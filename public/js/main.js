
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).on("click", ".follow-user", function () {
        var current = $(this);
        var user = $(this).attr('user');

        $.post("/user/follow",
            {
                user : user

            }, function (response) {
                if (response) {
                    current.removeClass('follow-user').addClass('unfollow-user');
                    current.find('.follow-text').text('Unfollow');
                    current.find('.follow-user-sign').text('-');
                }
            });
    });

    $(document).on("click", ".unfollow-user", function () {
        var current = $(this);
        var user = $(this).attr('user');

        $.post("/user/unfollow",
            {
                user : user

            }, function (response) {
                if (response) {
                    current.removeClass('unfollow-user').addClass('follow-user');
                    current.find('.follow-text').text('Follow');
                    current.find('.follow-user-sign').text('+');
                }
            });
    });

    $(document).on("click", ".tweet", function () {
        var tweet = $("#tweet").val();

        $.post("/home/tweet",
            {
                tweet : tweet

            }, function (response) {
                if (response) {
                    $("#tweet-modal").modal('hide');
                    $("#tweet").val('');
                    $(".tweet-list").html(response);
                }
            });
    });

    $(document).on("click", ".remove-tweet", function () {
        if(confirm('დარწმუნებული ხარ ?')){
            var tweet_id = $(this).attr('tweet_id');
            var current = $(this);

            $.post("/user/remove-tweet",
                {
                    tweet_id : tweet_id

                }, function (response) {
                    if (response) {
                        current.parent().parent().remove();
                    }
                });
        }
    });

    $(document).on("click", ".comment-tweet", function () {
            $(this).closest('.tweet-container').find('.tweet-container-comments').slideToggle();
    });

    $(document).on("click", ".submit-comment", function () {
            var id = $(this).attr('id');
            var tweet_id = $(this).attr('tweet_id');
            var current = $(this);
            var text = $(this).closest('.tweet-container-comments').find('textarea').val();

            $.post("/user/submit-comment",
                {
                    text : text,
                    tweet_id : tweet_id,
                    id : id

                }, function (response) {
                    if (response) {
                        $(".tweet-list").html(response);
                    }
                });
    });

    $(document).on("click", ".submit-comment-on-self-page", function () {
            var tweet_id = $(this).attr('tweet_id');
            var current = $(this);
            var text = $(this).closest('.tweet-container-comments').find('textarea').val();

            $.post("/home/submit-comment",
                {
                    text : text,
                    tweet_id : tweet_id

                }, function (response) {
                    if (response) {
                        $(".tweet-list").html(response);
                    }
                });
    });

});
