<?php

namespace App\Http\Controllers;

use DB;
use Request;


class SearchController extends Controller
{
    public function index()
    {
        // Gets the query string from our form submission
        $user = Request::input('user');
        $data= DB::table('users')->where(
            'firstname','like','%'.$user.'%'
        )->orWhere(
            'lastname','like','%'.$user.'%'
        )->get();

        return view('search',[
            'data' => $data
        ]);
    }
}
