<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Auth;
use PDO;
use Request;

use App\Http\Requests;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $data= DB::table('users')->where([
            'id'=>$id
        ])->first();

        if(!$data){
            return view('errors.404');
        }

        $followers = DB::table('follows')->where([
            'follower'=>Auth::id(),
            'following'=>$data['id'],
        ])->first();

        return view('user',[
            'currentUser'=>UserController::currentUser($id),
            'data' => $data,
            'tweets'=>$this->tweetList($id),
            'followers' => $followers,
            'id' => $id
        ]);
    }

    public static function tweetList($id)
    {
        $followings = [];

        $followings_ids = [];
        foreach($followings as $f){
            $followings_ids[] = $f['following'];
        }
        $followings_ids[] = $id; // იმ მომხმარებელთა ID-ები,რომელთა follower-იც არის + საკუთარი id

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $tweets = DB::table('tweets')
            ->select('tweets.id as id','tweets.author','tweets.text','tweets.date','users.firstname','users.lastname')
            ->join('users', 'tweets.author', '=', 'users.id')
            ->whereIn('author',$followings_ids) // საკუთარ tweet-ს + თავისი მეგობრების tweets
            ->orderBy('date','DESC')->get();

        $tweet_ids = [];
        foreach($tweets as $t){
            $tweet_ids[] = $t['id'];
        }

        $comment = DB::table('comments')
            ->select('tweet_id','text','author_id','date')
            ->whereIn('tweet_id',$tweet_ids)
            ->orderBy('comments.id','DESC')
            ->get();

        foreach($tweets as $key=>$t){
            // ვაკეთებთ tweet ების ქვემასივს 'comments' სადაც ვსვავთ მოცემული tweet ის კომენტარებს
            foreach($comment as $c){
                if($t['id'] == $c['tweet_id']){
                    $tweets[$key]['comments'][] = [
                        'text' => $c['text'],
                        'author_id' => $c['author_id'],
                        'date' => $c['date'],
                    ];
                }
            }
        }
        return $tweets;
    }

    public static function currentUser($id)
    {
        $currentUser = DB::table('users')
            ->where([
                'id'=>$id
            ])
            ->first();
        return $currentUser;

    }

    public function follow()
    {
        $follower = Auth::id();
        $following = Request::input('user');

        $query = DB::table('follows')->insert([
            'follower' => $follower,
            'following' => $following,
        ]);

        if($query){
            return 1;
        }
    }

    public function unfollow()
    {
        $follower = Auth::id();
        $following = Request::input('user');

        $query = DB::table('follows')->where([
            'follower' => $follower,
            'following' => $following,
        ]);

        if($query->delete()){
            return 1;
        }
    }



    public function removeTweet()
    {
        $author = Auth::id();
        $tweet_id= Request::input('tweet_id');

        $query = DB::table('tweets')->where([
            'author' => $author, // საჭირო გახდა დამატებით იმის შემოწმება არის თუ არა ეს tweet მიმდინარე user ის დამატებული,რათა არ მოხდეს მაგ.inspect-ით tweet_id-ს შეცვლა
            'id' => $tweet_id
        ]);

        if($query->delete()){
            return 1;
        }
    }

    public function submitComment()
    {
        $id = Request::input('id');
        $author = Auth::id();
        $tweet_id = Request::input('tweet_id');
        $text = Request::input('text');

        $query = DB::table('comments')->insert([
            'tweet_id' => $tweet_id,
            'author_id' => $author,
            'text' => $text
        ]);

        if($query){
            return view('tweetList',[
                'tweets'=>UserController::tweetList($id),
                'currentUser'=>UserController::currentUser($id),
                'id'=>$id
            ]);
        }
    }

}
