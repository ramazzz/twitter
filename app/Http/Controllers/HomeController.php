<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Request;
use DB;
use Illuminate\Support\Facades\Auth;
use PDO;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home',[
            'tweets'=>$this->tweetList(),
            'currentUser'=>$this->currentUser(),
        ]);
    }

    public function tweet()
    {
        $author = Auth::id();
        $tweet = Request::input('tweet');

        $query = DB::table('tweets')->insert([
            'author' => $author,
            'text' => $tweet,
        ]);

        if($query){
            return view('homeTweetList',[
                'tweets'=>$this->tweetList(),
                'currentUser'=>$this->currentUser(),
                'id'=>$author
            ]);
        }
    }

    public static function tweetList()
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $followings = DB::table('follows')->where([
            'follower'=>Auth::id(),
        ])->get();

        $followings_ids = [];
        foreach($followings as $f){
            $followings_ids[] = $f['following'];
        }
        $followings_ids[] = Auth::id(); // იმ მომხმარებელთა ID-ები,რომელთა follower-იც არის + საკუთარი id


        $tweets = DB::table('tweets')
            ->select('tweets.id as id','tweets.author','tweets.text','tweets.date','users.firstname','users.lastname')
            ->join('users', 'tweets.author', '=', 'users.id')
            ->whereIn('author',$followings_ids) // საკუთარ tweet-ს + თავისი მეგობრების tweets
            ->orderBy('date','DESC')->get();

        $tweet_ids = [];
        foreach($tweets as $t){
            $tweet_ids[] = $t['id'];
        }

        $comment = DB::table('comments')
                ->select('tweet_id','text','author_id','date')
                ->whereIn('tweet_id',$tweet_ids)
                ->orderBy('comments.id','DESC')
                ->get();

        foreach($tweets as $key=>$t){
            // ვაკეთებთ tweet ების ქვემასივს 'comments' სადაც ვსვავთ მოცემული tweet ის კომენტარებს
            foreach($comment as $c){
                if($t['id'] == $c['tweet_id']){
                    $tweets[$key]['comments'][] = [
                        'text' => $c['text'],
                        'author_id' => $c['author_id'],
                        'date' => $c['date'],
                    ];
                }
            }
        }

//        echo "<pre>";
//        print_R($tweets);
//        echo "</pre>";
//        exit;
        return $tweets;
    }

    public static function currentUser()
    {
        $currentUser = DB::table('users')
            ->where([
                'id'=>Auth::id()
            ])
            ->first();
        return $currentUser;

    }

    public function submitComment()
    {
        $id = Request::input('id');
        $author = Auth::id();
        $tweet_id = Request::input('tweet_id');
        $text = Request::input('text');

        $query = DB::table('comments')->insert([
            'tweet_id' => $tweet_id,
            'author_id' => $author,
            'text' => $text
        ]);

        if($query){
            return view('homeTweetList',[
                'tweets'=>$this->tweetList(),
                'currentUser'=>$this->currentUser()
            ]);
        }
    }


}
