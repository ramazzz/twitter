<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::post('/home', 'HomeController@index');

Route::get('/search','SearchController@index');

Route::get('/user/','HomeController@index');

Route::get('/user/{id}','UserController@index');

Route::post('/user/follow','UserController@follow');
Route::post('/user/unfollow','UserController@unfollow');
Route::post('/home/tweet','HomeController@tweet');
Route::post('/user/remove-tweet','UserController@removeTweet');
Route::post('/user/submit-comment','UserController@submitComment');
Route::post('/home/submit-comment','HomeController@submitComment');
//Route::auth();
//
//Route::get('/home', 'HomeController@index');
