@foreach($tweets as $t)

    <div class="tweet-container">
        <div class="tweet-container-header">
            <div class="tweet-container-avatar">
                <img src="/images/profile-small.png">
            </div>
            <div class="tweet-container-name">
                <a href="/user/{!! $t['author'] !!}">{!! $t['firstname'] !!} {!! $t['lastname']!!}</a>
            </div>
            <div class="tweet-container-nickname">
                {!! '@'.$t['firstname'] !!}{!! $t['lastname']!!}
            </div>
            <span class="glyphicon glyphicon-asterisk tweet-container-dot"></span>

            <div class="tweet-container-date">
                {!! $t['date']!!}
            </div>
        </div>
        <div class="tweet-container-content ">
            {!! $t['text'] !!}
        </div>
        <div class="tweet-container-options pull-right">
            <span class="glyphicon glyphicon-share-alt comment-tweet"></span>
            @if($t['author'] == Auth::id())
                <span class="glyphicon glyphicon-trash remove-tweet"
                      tweet_id="{!! $t['id'] !!}"></span>
            @endif
        </div>
        <div class="tweet-container-list">
            @if(isset($t['comments']))
                @foreach($t['comments'] as $comments)
                    <div class="comment-container">
                        {!! $comments['text'] !!}
                        <div class="pull-right">
                            {!! $comments['date'] !!}
                        </div>
                    </div>

                @endforeach
            @endif
        </div>
        <div class="tweet-container-comments">
            <textarea class="form-control tweet-comment" rows="5"></textarea>
            <button type="button" class="btn btn-primary submit-comment-on-self-page" tweet_id="{!! $t['id'] !!}">Add</button>
        </div>
    </div>
@endforeach