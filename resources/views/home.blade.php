@extends('layouts.app')

@section('content')

    <div class="cover-photo"></div>
    <div class="stats-bar-delimiter">
        <div>
            <div class="content">
                <div class="profile-photo">
                    <img src="images/profile.png"/>
                </div>

                <div class="stats-bar">
                    <div class="stats-bar-info">
                        <div class="col-sm-2 stats-bar-container">
                            <div class="stats-bar-info-title">TWEETS</div>
                            <div class="stats-bar-info-content">34.7K</div>
                        </div>
                        <div class="col-sm-2 stats-bar-container">
                            <div class="stats-bar-info-title">PHOTOS/VIDEOS</div>
                            <div class="stats-bar-info-content">295</div>
                        </div>
                        <div class="col-sm-2 stats-bar-container">
                            <div class="stats-bar-info-title">FOLLOWING</div>
                            <div class="stats-bar-info-content">685</div>
                        </div>
                        <div class="col-sm-2 stats-bar-container">
                            <div class="stats-bar-info-title">FOLLOWERS</div>
                            <div class="stats-bar-info-content">889</div>
                        </div>
                        <div class="col-sm-2 stats-bar-container">
                            <div class="stats-bar-info-title">FAVORITES</div>
                            <div class="stats-bar-info-content">247</div>
                        </div>
                        <div class="col-sm-2 stats-bar-container stats-bar-container-show">
                            <div class="col-sm-6">More</div>
                            <div class="col-sm-6">
                                <span class="glyphicon glyphicon-chevron-down stats-bar-container-show-arrow"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <div class="content-left">
                            <div class="profile-info">
                                <div class="profile-info-name">
                                    {!! $currentUser['firstname']!!}
                                    {!! $currentUser['lastname']!!}
                                </div>
                                <div class="profile-info-name-hashtag">
                                    {!! '@'.$currentUser['firstname']!!}{!! $currentUser['lastname']!!}
                                </div>
                                <div class="profile-info-about">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                </div>
                                <div class="profile-info-address">
                                    <span class="glyphicon glyphicon-map-marker"></span> City Road, London, UK
                                </div>
                                <div class="profile-info-web">
                                    <span class="glyphicon glyphicon-link"></span> <a href="http://everyinteraction.com"
                                                                                      target="_blank">everyinteraction.com</a>
                                </div>
                                <div class="profile-info-joined">
                                    <span class="glyphicon glyphicon-time"></span> Joined September 2007
                                </div>
                            </div>

                            <div id="tweet-modal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="tweet">Tweet:</label>
                                                <textarea class="form-control" rows="5" id="tweet"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="button" class="btn btn-primary tweet">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-tweet">
                                <div class="tweet-modal" data-toggle="modal" data-target="#tweet-modal">
                                    Add Tweet
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 tweet-list">
                        <?php
                        echo View::make('homeTweetList', [
                                'tweets' => $tweets,
                                'currentUser' => $currentUser,
                        ])
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="right-block">
                            <div class="right-block-title-container">
                                <span class="right-block-title">Who to follow</span>
                                <span class="glyphicon glyphicon-asterisk tweet-container-dot"></span>
                                <a>Refresh</a>
                                <span class="glyphicon glyphicon-asterisk tweet-container-dot"></span>
                                <a>View all</a>

                                <div class="right-block-user">
                                    <div class="right-block-item">
                                        <img src="/images/rightblockavatar1.png"/>
                                        <div class="pull-right">
                                            <span class="right-block-user-name">AppleInsider</span>
                                            <span class="right-block-user-name-hashtag">@appleinsider</span>
                                            <span class="glyphicon glyphicon-remove right-block-user-remove"></span>

                                            <div class="folowed-by">Followed by <a>Darkejon</a> and <a>others</a></div>

                                            <div class="right-block-follow-button">
                                                <span class="right-block-follow-button-blue">+</span>
                                            <span class="glyphicon glyphicon-user right-block-follow-button-blue">
                                                <span class="follow-text">
                                                    Follow
                                                </span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-block-item">
                                        <img src="/images/rightblockavatar2.png"/>
                                        <div class="pull-right">
                                            <span class="right-block-user-name">Creodo</span>
                                            <span class="right-block-user-name-hashtag">@creodo</span>
                                            <div class="glyphicon glyphicon-remove right-block-user-remove"></div>


                                            <div class="right-block-follow-button">
                                                <span class="right-block-follow-button-blue">+</span>
                                            <span class="glyphicon glyphicon-user right-block-follow-button-blue">
                                                <span class="follow-text">
                                                    Follow
                                                </span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-block-item">
                                        <img src="/images/rightblockavatar3.png"/>
                                        <div class="pull-right">
                                            <span class="right-block-user-name">AppleInsider</span>
                                            <span class="right-block-user-name-hashtag">@appleinsider</span>
                                            <span class="glyphicon glyphicon-remove right-block-user-remove"></span>


                                            <div class="right-block-follow-button">
                                                <span class="right-block-follow-button-blue">+</span>
                                            <span class="glyphicon glyphicon-user right-block-follow-button-blue">
                                                <span class="follow-text">
                                                    Follow
                                                </span>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

@endsection
